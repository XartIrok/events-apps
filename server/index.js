import express from 'express';
import mongodb from 'mongodb';
import bodyParser from 'body-parser';

const app = express();
const dbUrl = 'mongodb://localhost/events';

app.use(bodyParser.json());

function validate(data) {
  let errors = {};
  const patt = new RegExp('^[a-z0-9_\\-\\.]{2,}@[a-z0-9_\\-\\.]{2,}\\.[a-z]{2,}$');
  if (!patt.test(data.email)) errors.email = "Address email incorrect";
  if (data.firstName === '') errors.firstName = "Can't be empty";
  if (data.lastName === '') errors.lastName = "Can't be empty";
  if (data.email === '') errors.email = "Can't be empty";
  if (data.date === '') errors.date = "Can't be empty";
  const isValid = Object.keys(errors).length === 0;

  return { errors, isValid };
}

mongodb.MongoClient.connect(dbUrl, function(err, db) {
    if (err)
        console.log(err);

    app.get('/api/events', (req, res) => {
        db.collection('events').find({}).toArray((err, events) => {
            res.json({ events });
        });
    });

    app.post('/api/events', (req, res) => {
      const { errors, isValid } = validate(req.body);
      if (isValid) {
        const { firstName, lastName, email, date } = req.body;
        db.collection('events').insert({ firstName, lastName, email, date }, (err, result) => {
          if (err) {
            res.status(500).json({ errors: { global: 'Something went wrong'} })
          }
          else {
            res.json({ event: result.ops[0] });
          }
        });
      }
      else {
        res.status(400).json({ errors });
      }
    })

    app.put('/api/events/:_id', (req, res) => {
      const { errors, isValid } = validate(req.body);
      if (isValid) {
        const { firstName, lastName, email, date } = req.body;
        db.collection('events').findOneAndUpdate(
          { _id: new mongodb.ObjectId(req.params._id) },
          { $set: { firstName, lastName, email, date } },
          { returnOriginal: false },
          (err, result) => {
            if (err) {
              res.status(500).json({ errors: { global: 'Something went wrong'} })
            }
            else {
              res.json({ event: result.value });
            }
          }
        );
      }
      else {
        res.status(400).json({ errors });
      }
    });

    app.get('/api/events/:_id', (req, res) => {
      db.collection('events').findOne({ _id: new mongodb.ObjectId(req.params._id)}, (err, event) => {
        res.json({ event });
      });
    });

    app.delete('/api/events/:_id', (req, res) => {
      db.collection('events').deleteOne({ _id: new mongodb.ObjectId(req.params._id)}, (err, result) => {
        if (err) {
          res.status(500).json({ errors: { global: 'Something went wrong'} })
        }
        else {
          res.json({});
        }
      });
    });

    app.use((req, res) => {
      res.status(404).json({
        errors: {
          global: "Request API not found"
        }
      });
    });

    app.listen(8080, () => console.log('Server is start on localhost:8080'));
})
