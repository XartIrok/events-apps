import events from '../src/reducers/events';

describe('Reducer Test', () => {
  it('Default Reducer', () => {
    expect(events(undefined, { type: 'unexpected'})).toEqual([]);
  })

  it('EVENTS_SET Reducer', () => {
    expect(events(undefined, {
      type: 'EVENTS_SET',
      events: {
        firstName: 'Xart'
      }
    })).toEqual({ firstName: 'Xart' });
  })

  it('EVENT_ADD Reducer', () => {
    expect(events([], {
      type: 'EVENT_ADD',
      event: {
        firstName: 'Xart'
      }
    })).toEqual([{ firstName: 'Xart' }]);
  })

  it('EVENT_ADD Reducer handle', () => {
    expect(events([
      {
        firstName: 'Test'
      }
    ], {
      type: 'EVENT_ADD',
      event: {
        firstName: 'Xart'
      }
    })).toEqual([{ firstName: 'Test' }, { firstName: 'Xart' }]);
  })
})
