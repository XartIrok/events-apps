import React from 'react';
import classnames from 'classnames';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';

class EventForm extends React.Component {
  state = {
    _id: this.props.event ? this.props.event._id : null,
    firstName: this.props.event ? this.props.event.firstName : '',
    lastName: this.props.event ? this.props.event.lastName : '',
    email: this.props.event ? this.props.event.email : '',
    date: this.props.event ? this.props.event.date : moment(),
    errors: {}
  }

  componentWillReceiveProps = (nextProps) => {
    this.setState({
      _id: nextProps.event._id,
      firstName: nextProps.event.firstName,
      lastName: nextProps.event.lastName,
      email: nextProps.event.email,
      date: nextProps.event.date
    })
  }

  handleChange = (e) => {
    if (!!this.state.errors[e.target.name]) {
      let errors = Object.assign({}, this.state.errors);
      delete errors[e.target.name];
      this.setState({
        [e.target.name]: e.target.value,
        errors
      });
    }
    else {
      this.setState({[e.target.name]: e.target.value});
    }
  }

  handleChangeSecond = (e) => {
    // const dateEvent = moment(e).format('YYYY-MM-DD');
    this.setState({date: e});
  }

  handleSubmit = (e) => {
    e.preventDefault();
    let errors = {};
    const patt = new RegExp('^[a-z0-9_\\-\\.]{2,}@[a-z0-9_\\-\\.]{2,}\\.[a-z]{2,}$');
    if (!patt.test(this.state.email)) errors.email = "Address email incorrect";
    if (this.state.firstName === '') errors.firstName = "Can't be empty";
    if (this.state.lastName === '') errors.lastName = "Can't be empty";
    if (this.state.email === '') errors.email = "Can't be empty";
    if (this.state.date === '') errors.date = "Can't be empty";
    this.setState({ errors })
    const isValid = Object.keys(errors).length === 0;

    if (isValid) {
      const { _id, firstName, lastName, email, date } = this.state;
      this.props.saveEvent({ _id, firstName, lastName, email, date })
      .catch((err) => err.respone.json().then(({errors}) => this.setState({errors})));
    }
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <h2>{this.props.event ? 'Edit event' : 'Add new event' }</h2>
        {!!this.state.errors.global && <div className="alert alert-danger" role="alert">{this.state.errors.global}</div>}
        <div className="form-group row">
          <label htmlFor="first-name" className="col-2 col-form-label">First Name</label>
          <div className={classnames("col-4", { 'has-danger': !!this.state.errors.firstName})}>
            <input
              className={classnames("form-control", { 'form-control-danger': !!this.state.errors.firstName})}
              id="first-name"
              name="firstName"
              value={this.state.firstName}
              onChange={this.handleChange}
            />
            <div className="form-control-feedback">{this.state.errors.firstName}</div>
          </div>
          <label htmlFor="last-name" className="col-2 col-form-label">Last Name</label>
          <div className={classnames("col-4", { 'has-danger': !!this.state.errors.lastName})}>
            <input
              className={classnames("form-control", { 'form-control-danger': !!this.state.errors.lastName})}
              id="last-name"
              name="lastName"
              value={this.state.lastName}
              onChange={this.handleChange}
            />
            <div className="form-control-feedback">{this.state.errors.lastName}</div>
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="email" className="col-2 col-form-label">Email</label>
          <div className={classnames("col-4", { 'has-danger': !!this.state.errors.email})}>
            <input
              className={classnames("form-control", { 'form-control-danger': !!this.state.errors.email})}
              id="email"
              name="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
            <div className="form-control-feedback">{this.state.errors.email}</div>
          </div>
          <label htmlFor="date" className="col-2 col-form-label">Date Event</label>
          <div className={classnames("col-4", { 'has-danger': !!this.state.errors.date})}>
            <DatePicker
              className={classnames("form-control", { 'form-control-danger': !!this.state.errors.date})}
              id="date"
              name="date"
              dateFormat="YYYY-MM-DD"
              locale="en-gb"
              minDate={moment()}
              selected={this.state.date}
              onChange={this.handleChangeSecond}
            />
            <div className="form-control-feedback">{this.state.errors.date}</div>
          </div>
        </div>
        <div className="form-group row">
          <div className="offset-sm-2 col-sm-10">
            <button type="submit" className="btn btn-primary">Save</button>
          </div>
        </div>
      </form>
    )
  }
}

export default EventForm;
