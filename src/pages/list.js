import React from 'react';
import EventCard from './eventCard';

export default function EventsList({ events, deleteEvent }) {
    const emptyMessage = (
        <p>No events</p>
    );

    const eventsList = (
      <div className="row">
        { events.map(event => <EventCard event={event} key={event._id} deleteEvent={deleteEvent} />)}
      </div>
    );

    return (
        <div>
        { events.length === 0 ? emptyMessage : eventsList }
        </div>
    );
}

EventsList.propTypes = {
    events: React.PropTypes.array.isRequired,
    deleteEvent: React.PropTypes.func.isRequired
}
