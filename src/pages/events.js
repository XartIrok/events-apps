import React from 'react';
import EventsList from './list';
import { connect } from 'react-redux';
import { fetchEvents } from '../actions/events';
import { deleteEvent } from '../actions/event';

class EventsPage extends React.Component {
    componentDidMount() {
        this.props.fetchEvents();
    }

    render() {
        return (
            <div>
                <h2>Events List</h2>

                <EventsList events={this.props.events} deleteEvent={this.props.deleteEvent} />
            </div>
        );
    }
}

EventsPage.propTypes = {
    events: React.PropTypes.array.isRequired,
    fetchEvents: React.PropTypes.func.isRequired,
    deleteEvent: React.PropTypes.func.isRequired
}

function mapStateToProps(state) {
    return {
        events: state.events
    }
}

export default connect(mapStateToProps, { fetchEvents, deleteEvent })(EventsPage);
