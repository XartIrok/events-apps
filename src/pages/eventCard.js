import React from 'react';
import { Link } from 'react-router-dom';

export default function EventCard({ event, deleteEvent }) {

    return (
      <div className="col col-4">
        <div className="card">
          <div className="card-header">
            {event.date}
          </div>
          <div className="card-block">
            <h4 className="card-title">Card title event</h4>
            <p className="card-text">desc event</p>
            <p className="card-text">Contact with organizer: {event.email}</p>
          </div>

          <div className="btn-group">
            <Link to={`/event/${ event._id }`} role="button" className="btn btn-success col">Edit</Link>
            <div role="button" className="btn btn-danger col" onClick={() => deleteEvent(event._id)}>Delete</div>
          </div>
          <div className="card-footer text-muted">
            Author: {event.firstName} {event.lastName}
          </div>
        </div>
      </div>
    );
}

EventCard.propTypes = {
    event: React.PropTypes.object.isRequired,
    deleteEvent: React.PropTypes.func.isRequired
}
