import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { saveEvent, fetchEvent, updateEvent } from '../actions/event';
import EventForm from './eventForm';

class EventFormPage extends React.Component {
  state = {
    redirect: false
  }

  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchEvent(match.params._id);
    }
  }

  eventSave = ({_id, firstName, lastName, email, date }) => {
    if (_id) {
      return this.props.updateEvent({ _id, firstName, lastName, email, date }).then(
        () => { this.setState({ redirect: true })},
      );
    } else {
      return this.props.saveEvent({ firstName, lastName, email, date }).then(
        () => { this.setState({ redirect: true })},
      );
    }
  }

  render() {
    return (
      <div>
        {
          this.state.redirect ?
          <Redirect to="/events" /> :
          <EventForm
            event={this.props.event}
            saveEvent={this.eventSave}
          />
        }
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      event: state.events.find(item => item._id === match.params._id)
    }
  }

  return { event: null };
}

export default connect(mapStateToProps, { saveEvent, fetchEvent, updateEvent })(EventFormPage);
