export default function events(state = [], action = {}) {
    switch (action.type) {
        case 'EVENTS_SET':
            return action.events;
        case 'EVENT_ADD':
          return [
            ...state,
            action.event
          ];
        case 'EVENT_DELETED':
          return state.filter(item => item._id !== action.eventId);
        case 'EVENT_UPDATED':
          return state.map(item => {
            if (item._id === action.event._id) return action.event;
            return item;
          });
        case 'EVENT_FETCHED':
          const index = state.findIndex(item => item._id === action.event._id);
          if (index > -1) {
            return state.map(item => {
              if (item._id === action.event._id) return action.event;
              return item;
            });
          }
          else {
            return [
              ...state,
              action.event
            ]
          }
        default:
            return state;
    }
}
