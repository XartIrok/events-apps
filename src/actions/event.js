function handleResponse(response) {
  if (response.ok) {
    return response.json();
  }
  else {
    let error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
}

export function addEvent(event) {
  return {
    type: 'EVENT_ADD',
    event
  }
}

export function eventFetched(event) {
  return {
    type: 'EVENT_FETCHED',
    event
  }
}

export function eventUpdated(event) {
  return {
    type: 'EVENT_UPDATED',
    event
  }
}

export function eventDeleted(eventId) {
  return {
    type: 'EVENT_DELETED',
    eventId
  }
}

export function updateEvent(data) {
  return dispatch => {
    return fetch(`/api/events/${data._id}`, {
      method: 'put',
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json"
      }
    }).then(handleResponse)
    .then(data => dispatch(eventUpdated(data.event)));
  }
}

export function deleteEvent(id) {
  return dispatch => {
    return fetch(`/api/events/${id}`, {
      method: 'delete',
      headers: {
        "Content-Type": "application/json"
      }
    }).then(handleResponse)
    .then(data => dispatch(eventDeleted(id)));
  }
}

export function saveEvent(data) {
  return dispatch => {
    return fetch('/api/events', {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json"
      }
    }).then(handleResponse)
    .then(data => dispatch(addEvent(data.event)));
  }
}

export function fetchEvent(id) {
  return dispatch => {
    return fetch(`/api/events/${id}`)
    .then(res => res.json())
    .then(data => dispatch(eventFetched(data.event)));
  }
}
