export function setEvents(events) {
    return {
        type: 'EVENTS_SET',
        events
    }
}

export function fetchEvents() {
    return dispatch =>{
        fetch('/api/events')
            .then(res => res.json())
            .then(data => dispatch(setEvents(data.events)));
    }
}
