import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';
import MainPage from './pages/main';
import EventsPage from './pages/events';
import EventFormPage from './pages/eventFormPage'
import './App.css';

const ActiveLink = ({ label, to, activeOnlyWhenExact }) => (
  <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => (
    <Link className={match ? 'active nav-link' : 'nav-link'} to={to}>{label}</Link>
  )} />
);

class App extends Component {
  render() {
    return (
      <div className="container">
        <nav className="navbar navbar-toggleable-md navbar-light bg-faded">
          <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <a className="navbar-brand" href="#">&nbsp;</a>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <ActiveLink activeOnlyWhenExact to="/" label="Home" />
              </li>
              <li className="nav-item">
                <ActiveLink activeOnlyWhenExact to="/events" label="Events" />
              </li>
              <li className="nav-item">
                <ActiveLink activeOnlyWhenExact to="/events/new" label="New Events" />
              </li>
            </ul>
          </div>
        </nav>
        <div className="jumbotron">
          <Route exact path="/" component={MainPage} />
          <Route exact path="/events" component={EventsPage} />
          <Route exact path="/events/new" component={EventFormPage} />
          <Route exact path="/event/:_id" component={EventFormPage} />
        </div>
      </div>
    );
  }
}

export default App;
